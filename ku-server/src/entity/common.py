#coding=utf-8
from dataclasses import fields
from sqlalchemy.dialects import mysql

from .base import Base, ModelBase
from sqlalchemy import Column, String, text


class User(Base,ModelBase):
	__tablename__ = 'user'
	__table_args__ = {
		'mysql_engine': 'InnoDB',
		'mysql_charset': 'utf8mb4',
		'mysql_collate': 'utf8mb4_general_ci',
		'mysql_row_format': 'Dynamic',
		'mysql_auto_increment': '1',
		'comment': '用户表',
	}
	
	user_name = Column(String(100), nullable=False, server_default='', comment='用户名')
	user_nack_name = Column(String(100), nullable=False, server_default='', comment='昵称')
	pwd = Column(String(100), nullable=False, server_default='', comment='密码')
	
