# coding=utf-8
import hashlib
import inspect
import json
import random
from typing import Optional

import global_var
from base.base import AppException
from base.http_base import HttpResp
from server_module import misc_logic

from utils import proto_util


async def send_dict(user_id, data_dict: dict, force_send=False):
	if force_send:
		from server_module.server import ConnMgr
		await ConnMgr.get().send_data(user_id, json.dumps(data_dict))
	else:
		from server_module import handler
		handler.g_msg_list.append((user_id, data_dict))


async def send_data(user_id, type, func, data_dict: dict, force_send=False):
	data_dict = proto_util.to_json_dict(data_dict)
	data_dict["type"] = type
	data_dict["func"] = func
	await send_dict(user_id, data_dict, force_send)


async def close_user(user_id):
	from mgr.user_mgr import UserMgr
	from server_module.server import ConnMgr
	await ConnMgr.get().close_conn(user_id)
	await UserMgr.get().removeUser(user_id)


async def raise_exception(user_id, msg):
	await misc_logic.send_tip(user_id, msg, True)
	raise AppException(HttpResp.FAILED, msg=msg)


def is_empty(obj):
	if obj is None:
		return True
	if type(obj) is str and obj == "":
		return True
	if type(obj) is list and len(obj) == 0:
		return True
	return False

