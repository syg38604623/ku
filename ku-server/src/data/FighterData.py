# coding=utf-8
from typing import Dict, Any


class FighterDataObject:
	num: int
	fighterName: str
	maxShieldEnergy: int
	maxMoveSpeed: int
	slowRotSpeed: int
	fastRotSpeed: int
	bigShotEnergyAddSpeed: int
	fireRate: int
	color: str

	def __init__(self, *args, **kwargs):
		for key, value in kwargs.items():
			setattr(self, key, value)


data: Dict[int, FighterDataObject] = {
	0: FighterDataObject(
		num=0,
		fighterName="黄金之翼",
		maxShieldEnergy=140,
		maxMoveSpeed=10,
		slowRotSpeed=14,
		fastRotSpeed=24,
		bigShotEnergyAddSpeed=5,
		fireRate=0.1,
		color="#FFB800",
	),
	1: FighterDataObject(
		num=1,
		fighterName="蓝色闪电",
		maxShieldEnergy=120,
		maxMoveSpeed=12,
		slowRotSpeed=16,
		fastRotSpeed=24,
		bigShotEnergyAddSpeed=5,
		fireRate=0.1,
		color="#008FFF",
	),
	2: FighterDataObject(
		num=2,
		fighterName="紫云幽影",
		maxShieldEnergy=120,
		maxMoveSpeed=10,
		slowRotSpeed=14,
		fastRotSpeed=24,
		bigShotEnergyAddSpeed=10,
		fireRate=0.1,
		color="#D600FF",
	),
}
