# coding=utf-8
import random


def get_random_name():
	surname = ["西本", "日高", "筱冢", "绿谷", "毛利", "月咏", "工藤", "汤川", "金木",
	           "加贺", "内海", "日暮", "夏目", "珍野", "道木", "铃原"]
	
	name = ["雪穗", "邦彦", "一成", "出久", "奈美江", "几斗", "新一", "三叶", "研", "唯",
	        "熏", "玲", "篱", "兰", "恭一郎", "修", "寒月", "贵志", "苦沙弥", "怜南", "奈绪"]
	index = random.randint(0, len(surname) - 1)
	
	somesurname = surname[index]
	index = random.randint(0, len(name) - 1)
	
	somename = name[index]
	return somesurname + somename
