# coding=utf-8

import misc
from common import defines


async def send_tip(user_id, msg, force_send=False):
	await misc.send_data(user_id, defines.S2C_COMMON, "openBubbleTipDlg", {
		"msg": msg,
	}, force_send)

async def send_user_nack_name(user_id, user_nack_name):
	await misc.send_data(user_id, defines.S2C_COMMON, "recvUserNackName", {
		"user_nack_name": user_nack_name,
	})