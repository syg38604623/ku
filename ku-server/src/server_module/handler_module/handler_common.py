# coding=utf-8
import random

import misc
from base.singleton import SingletonBase
from common import defines
from entity.common import User
from mgr.user_mgr import UserMgr
from server_module import misc_logic
from server_module.sender import sender_common
from sql import sqlplus
from sql.database import db
from utils import datetime_util


async def handle(data: dict):
	obj = HandleCommon.get()
	await getattr(obj, data["func"])(data["user_id"], data)


class HandleCommon(SingletonBase):
	async def heart(self, user_id, data):
		await UserMgr.get().heartUser(user_id)

	async def recv_user_nack_name(self, user_id, data):
		user_nack_name = data["user_nack_name"]
		if misc.is_empty(user_nack_name):
			await misc.raise_exception(user_id, "用户昵称为空")

		await sqlplus.update(User, {
			"id": user_id,
			"user_nack_name": user_nack_name
		})

		await sender_common.send_user_nack_name(user_id, user_nack_name)

