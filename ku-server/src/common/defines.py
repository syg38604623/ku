# coding=utf-8

FLAG_NO = 0
FLAG_YES = 1

C2S_COMMON = 0
C2S_USER = 1
C2S_HERO = 2
C2S_FIGHT = 3

S2C_COMMON = 0
S2C_USER = 1
S2C_HERO = 2
S2C_FIGHT = 3

S2C_TEST = 99

MAX_HERO_GRADE = 100
MAX_USER_GRADE = 100

ITEM_SHENSHI = 4  # 神石
MAX_TEAM_HERO = 6  # 阵容最大英雄数
LONG_MXA = 2 ** 53 - 1
LONG_MIN = -2 ** 53 + 1
OFFSET_RUNE2ITEM = 1000  # 符文对应的物品编号偏移

COIN_SILVER = 0
COIN_GOLD = 1

TASK_1 = 1  # 1 - 累计角色达到级别

TASK_2 = 2  # 2 - 通关挑战对应关

TASK_3 = 3  # 3 - 累计拥有角色数

MAX_GRADE = 100  # 角色最大等级

ROOM_MATCH = 1

COMPENSATE_DEBRIS = 20  # 补偿碎片数量

FIGHT_TYPE_SLEEP = 0  # 挂机
FIGHT_TYPE_LEVEL = 1  # 关卡战斗
FIGHT_TYPE_CHALLENGE = 2  # 副本
FIGHT_TYPE_PVP = 3  # pvp


TEAM_SIZE = 6

MUL_FLAG_0 = 0  # 单体

MUL_FLAG_1 = 1  # 群体

MUL_FLAG_2 = 2  # 多段

EFFECT_1 = 1  # 加回避

EFFECT_2 = 2  # 封印

EFFECT_3 = 3  # 吸血

EFFECT_4 = 4  # 减伤

EFFECT_5 = 5  # 治愈

EFFECT_6 = 6  # 群体封印

EFFECT_7 = 7  # 重伤

ITEM_DICT = {
	0: {
		"key": "coin",
		"obj_name": "金币",
	},
	1: {
		"key": "crystal",
		"obj_name": "水晶",
	},
	2: {
		"key": "debris",
		"obj_name": "碎片",
	},
}
