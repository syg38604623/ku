﻿import { _decorator, Component, Node, __private } from 'cc';
import { PropBase } from '../base/PropBase';


const { ccclass, property } = _decorator;

@ccclass('UserProp')
export class UserProp extends PropBase {
    static ins: UserProp;
    static init() {
        UserProp.ins = new UserProp()
    }
    id: string = "";
    user_name: string = "";
    user_nack_name: string = "";
}

