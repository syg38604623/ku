﻿import { _decorator, Component, Node } from 'cc';
import { ComBase } from '../base/ComBase';
import { SysBase } from '../base/SysBase';
import { KeyCom } from '../common/com/KeyCom';

const { ccclass, property } = _decorator;

@ccclass('CommonSys')
export class CommonSys extends SysBase {
    onLoad() {
        this.addCom(KeyCom);
    }

}

