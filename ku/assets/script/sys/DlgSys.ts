﻿import { __private, _decorator, Component, Node } from 'cc';
import { ComBase } from '../base/ComBase';
import { DlgBase } from '../base/DlgBase';
import { SysBase } from '../base/SysBase';
import { DlgMgrCom } from '../dlg/mgrcom/DlgMgrCom';

const { ccclass, property } = _decorator;

@ccclass('DlgSys')
export class DlgSys extends SysBase {
    onLoad() {
        this.addCom(DlgMgrCom);
    }
    closeDlgByPath(UI_PATH: string): void {
        this.getCom(DlgMgrCom).closeDlgByPath(UI_PATH);
    }


    closeMulDlg(node: Node): void {
        this.getCom(DlgMgrCom).closeMulDlg(node);
    }

    async createMulPoolDlg(dlgConstructor: typeof DlgBase, data?: any): Promise<Node> {
        return await this.getCom(DlgMgrCom).createMulPoolDlg(dlgConstructor, data);
    }

    async createMulDlg(dlgConstructor: typeof DlgBase, data?: any): Promise<Node> {
        return await this.getCom(DlgMgrCom).createMulDlg(dlgConstructor, data);
    }

    async createDlg(dlgConstructor: typeof DlgBase, data?: any): Promise<Node> {
        return await this.getCom(DlgMgrCom).createDlg(dlgConstructor, data);
    }

    getDlgCom<T extends DlgBase>(dlgConstructor: __private._types_globals__Constructor<T>): T | null {
        return this.getCom(DlgMgrCom).getDlgCom(dlgConstructor);
    }


    getDlg(dlgConstructor: typeof DlgBase): Node {
        return this.getCom(DlgMgrCom).getDlg(dlgConstructor);
    }

    getDlgByName(dlgName: string): Node {
        return this.getCom(DlgMgrCom).getDlgByName(dlgName);
    }

    hideDlg(dlgConstructor: typeof DlgBase): void {
        return this.getCom(DlgMgrCom).hideDlg(dlgConstructor);
    }

    hideDlgByPath(UI_PATH: string): void {
        return this.getCom(DlgMgrCom).hideDlgByPath(UI_PATH);
    }

    closeDlg(dlgConstructor: typeof DlgBase): void {
        this.getCom(DlgMgrCom).closeDlg(dlgConstructor);
    }

}