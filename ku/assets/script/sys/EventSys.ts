﻿import { _decorator, Component, Node, AudioClip } from 'cc';
import { SysBase } from '../base/SysBase';
import { DestroyCB } from '../common/DestroyCB';
import { EventDefine } from '../common/EventDefine';
import { EventProp } from '../prop/EventProp';

const { ccclass, property } = _decorator;

@ccclass('EventSys')
export class EventSys extends SysBase {
    eventProp: EventProp;
    eventList = [
        EventDefine.PLAYER_RECORD_CHANGE,
    ]
    onLoad() {
        this.eventProp = new EventProp();
        for (var event of this.eventList) {
            this.eventProp[event] = 0;
        }
    }

    watchEvent(event: string, obj: any, funcName: string) {
        this.eventProp.watch(event, obj, funcName);
    }

    async runEvent(event: string, args?: any) {
        var obj = new Object();
        obj[event] = this.eventProp[event] + 1;
        await this.eventProp.setData(obj, args);
    }
  
}

