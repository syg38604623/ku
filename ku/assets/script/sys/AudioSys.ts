﻿import { _decorator, Component, Node, AudioClip, Vec3 } from 'cc';
import { ComBase } from '../base/ComBase';
import { SysBase } from '../base/SysBase';
import { GlobalVar } from '../common/GlobalVar';
import { UserProp } from '../prop/UserProp';
import { ResourceLoader } from '../common/ResourceLoader';
import { SysMgr } from '../common/SysMgr';
import { PoolSys } from './PoolSys';
import { TimeSys } from './TimeSys';

const { ccclass, property } = _decorator;

@ccclass('AudioSys')
export class AudioSys extends SysBase {
    audioMap = new Map<string, AudioClip>();
    audioVolumeRatio = 1;//统一音量系数
    wxAudioMap = new Map<string, any>();
    isOpenAudio = true;
    heroProp: UserProp;
    audioCount = new Map<string, number>();

    data: Map<string, number> = new Map([
        ["audios/touch.mp3", 1],
    ])
    
    async preLoad() {

        for (var path of this.data.keys()) {
            let clip = await ResourceLoader.loadAsset<AudioClip>(path);
            this.audioCount.set(path, 0);
            this.audioMap.set(path, clip);
            this.wxAudioMap.set(path, clip);
        }
    }

    stopAudio(path: string) {

        if (GlobalVar.isWX) {
            let innerAudioContext = this.wxAudioMap.get(path);
            innerAudioContext.pause();
            innerAudioContext.stop();
        } else {
            let audioClip = this.audioMap.get(path);
            if (audioClip == null)
                return;
            audioClip.pause();
            audioClip.stop();
        }


    }




    async playAudio(path: string, ratio?: number, volume?: number, loop?: boolean) {
        if (!this.isOpenAudio) return;
        if (GlobalVar.isWX) {
            await this.playWXAudio(path, ratio, volume, loop);
        } else {
            await this.playNormalAudio(path, ratio, volume, loop);
        }
        
    }
    async playWXAudio(path: string, ratio?: number, volume?: number, loop?: boolean) {
        let clip;
        if (!this.wxAudioMap.has(path)) {
            clip = await ResourceLoader.loadAsset<AudioClip>(path);
            this.wxAudioMap.set(path, clip);
        }
        if (volume == null) {
            volume = this.data.get(path);
        }
        if (ratio == null) {
            ratio = 1;
        }

        clip = this.wxAudioMap.get(path);
        let innerAudioContext = window["wx"].createInnerAudioContext({ useWebAudioImplement: true });
        innerAudioContext.src = clip.nativeUrl;

        if (loop) {

            innerAudioContext.loop = true
        } else {
            innerAudioContext.loop = false
        }

        if (volume != null)
            innerAudioContext.volume = volume * this.audioVolumeRatio * ratio;
        else
            innerAudioContext.volume = this.audioVolumeRatio * ratio;
        innerAudioContext.play();
    }

    getWxAudio(url:string) {

    }

    async playNormalAudio(path: string, ratio?: number, volume?: number, loop?: boolean) {
        if (!this.audioMap.has(path)) {
            var clip = await ResourceLoader.loadAsset<AudioClip>(path);
            this.audioCount.set(path, 0);
            this.audioMap.set(path, clip);
        }
        if (volume == null) {
            volume = this.data.get(path);
        }
        if (ratio == null) {
            ratio = 1;
        }

        let count = this.audioCount.get(path)
        if (count >= 3) {
            return;
        }
        let audioClip = this.audioMap.get(path);


        if (loop) {
            audioClip.setLoop(true)

            if (volume != null)
                audioClip.setVolume(volume * this.audioVolumeRatio * ratio);
            else
                audioClip.setVolume(this.audioVolumeRatio * ratio);
            audioClip.play();
        } else {
            audioClip.setLoop(false)
            if (volume != null)
                audioClip.playOneShot(volume * this.audioVolumeRatio * ratio);
            else
                audioClip.playOneShot(this.audioVolumeRatio * ratio);
        }



        count++;
        this.audioCount.set(path, count);
        SysMgr.getSys(TimeSys).addTimer(this, "delayCountDown", 0.2, false, path)
 
    }
    delayCountDown(path: string) {
        let count = this.audioCount.get(path)
        count--;
        this.audioCount.set(path, count);
    }

}

