import { _decorator, Component, Node, loader, sys, log } from 'cc';
import { Misc } from '../common/Misc';
const { ccclass, property } = _decorator;

@ccclass('RandomUtils')
export class RandomUtils {
    static randomNum(min: any, max: any) {//不包括max
        return Math.floor(Math.random() * (max - min) + min)
    }
    static randomBoundary(minLeft: any, maxLeft: any, minRight: any, maxRight: any) {//不包括max
        if (RandomUtils.randomBool()) {
            return RandomUtils.randomNum(minLeft, maxLeft);
        }
        return RandomUtils.randomNum(minRight, maxRight);
    }
    static randomString(length: number): string {
        return Math.random()
            .toString(36)
            .replace(/[^a-zA-Z0-9]+/g, '')
            .substr(0, length);
    }
    static randomBool(): boolean {
        if (RandomUtils.randomNum(0, 2) == 1)
            return true;
        return false;
    }
    static randomList(min: number, max: number, len: number) {//返回列表 他的元素在min-max之间（不包括max） 元素个数为len
        var list = [];
        for (var i = min; i < max; i++) {
            list.push(i);
        }
        var result = [];
        for (var i = 0; i < len; i++) {
            var index = RandomUtils.randomNum(0, max - min - i);
            result.push(Misc.removeListByIndex<number>(index, list))
        }
        return result;
    }
    static randomListByList(list, len: number) {//返回列表中随机len个元素
        var result = [];
        var lst = [];
        lst.push(...list)
        var num = list.length;
        for (var i = 0; i < len; i++) {
            var index = RandomUtils.randomNum(0, num - i);
            result.push(Misc.removeListByIndex<number>(index, lst))
        }
        return result;
    }
    static randomName() {
        let surname = ["西本", "日高", "筱冢", "绿谷", "毛利", "月咏", "工藤", "汤川", "金木",
            "加贺", "内海", "日暮", "夏目", "珍野", "道木", "铃原"]
        let name = ["雪穗", "邦彦", "一成", "出久", "奈美江", "几斗", "新一", "三叶", "研", "唯",
            "熏", "玲", "篱", "兰", "恭一郎", "修", "寒月", "贵志", "苦沙弥", "怜南", "奈绪"]
        let index = this.randomNum(0, surname.length);
        let somesurname = surname[index]
        index = this.randomNum(0, surname.length);
        let somename = name[index]
        return somesurname + somename
    }

}

