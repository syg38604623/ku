import { _decorator, Node, __private} from 'cc';
import { FighterDataObject } from './FighterDataObject';
const { ccclass, property } = _decorator;

@ccclass('FighterData')
export class FighterData{
	static data: Map<number, FighterDataObject> = new Map([
		[0, {
			num: 0,
			fighterName: "黄金之翼",
			maxShieldEnergy: 140,
			maxMoveSpeed: 10,
			slowRotSpeed: 14,
			fastRotSpeed: 24,
			bigShotEnergyAddSpeed: 5,
			fireRate: 0.1,
			color: "#FFB800",
		}],
		[1, {
			num: 1,
			fighterName: "蓝色闪电",
			maxShieldEnergy: 120,
			maxMoveSpeed: 12,
			slowRotSpeed: 16,
			fastRotSpeed: 24,
			bigShotEnergyAddSpeed: 5,
			fireRate: 0.1,
			color: "#008FFF",
		}],
		[2, {
			num: 2,
			fighterName: "紫云幽影",
			maxShieldEnergy: 120,
			maxMoveSpeed: 10,
			slowRotSpeed: 14,
			fastRotSpeed: 24,
			bigShotEnergyAddSpeed: 10,
			fireRate: 0.1,
			color: "#D600FF",
		}],
	])
}
