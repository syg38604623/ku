import { _decorator, Node, __private} from 'cc';
const { ccclass, property } = _decorator;

@ccclass('FighterDataObject')
export class FighterDataObject{
	num: number;
	fighterName: string;
	maxShieldEnergy: number;
	maxMoveSpeed: number;
	slowRotSpeed: number;
	fastRotSpeed: number;
	bigShotEnergyAddSpeed: number;
	fireRate: number;
	color: string;
}
