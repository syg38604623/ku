﻿import { _decorator, Component, Node, input, Input, EventKeyboard, KeyCode, loader, assetManager, Texture2D, SpriteFrame, Sprite } from 'cc';
import { UserProp } from '../../prop/UserProp';
import { SysMgr } from '../../common/SysMgr';
import { WsSys } from '../../sys/WsSys';
import { Defines } from '../../common/Defines';




const { ccclass, property } = _decorator;

@ccclass('NSendCommon')
export class NSendCommon {
    static send_user_nack_name(user_nack_name) {
        SysMgr.getSys(WsSys).sendMsg(Defines.C2S_COMMON, "recv_user_nack_name", {
            user_nack_name: user_nack_name
        });
    }

   
}

