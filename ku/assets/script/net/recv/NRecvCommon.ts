﻿import { _decorator, Component, Node, input, Input, EventKeyboard, KeyCode, loader, assetManager, Texture2D, SpriteFrame, Sprite } from 'cc';
import { UserProp } from '../../prop/UserProp';
import { BubbleTipDlgCom } from '../../dlg/dlgcom/BubbleTipDlgCom';




const { ccclass, property } = _decorator;

@ccclass('NRecvCommon')
export class NRecvCommon {
    static async recvUserNackName(data) {
        UserProp.ins.setValue("user_nack_name", data.user_nack_name)
    }

    static async connSucc(data) {

    }

}

