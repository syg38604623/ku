﻿import { _decorator, Component, Node, __private, Vec3, Vec2, Widget, Enum } from 'cc';
import { ComBase } from '../../base/ComBase';
import { DlgBase } from '../../base/DlgBase';
import { GlobalVar } from '../../common/GlobalVar';
import { ResourceLoader } from '../../common/ResourceLoader';
import { ResourcesPathDefine } from '../../common/ResourcesPathDefine';
import { PoolSys } from '../../sys/PoolSys';
import { SysMgr } from '../../common/SysMgr';

const { ccclass, property } = _decorator;

@ccclass('DlgMgrCom')
export class DlgMgrCom extends ComBase {
    
    dlgMap = new Map<string, Node>();
    root: Node;
    topRoot: Node;
    poolSys: PoolSys
    onLoad() {
        this.poolSys = SysMgr.getSys(PoolSys)
        this.root = GlobalVar.canvas.getChildByName("uiRoot");
    }

    closeMulDlg(node: Node): void {

        if (node["__is_pool"]) {
            this.poolSys.destroyNode(node)
        } else {
            node.destroy();
        }
    }

    async createMulDlg(dlgConstructor: typeof DlgBase, data?: any): Promise<Node> {
        var obj = await ResourceLoader.insNode(ResourcesPathDefine.UI_ROOT_PATH + "/" + dlgConstructor.UI_PATH)
        obj.addComponent(dlgConstructor).initDlg(data);
        obj.parent = this.root;
        return obj;
    }

    async createMulPoolDlg(dlgConstructor: typeof DlgBase, data?: any): Promise<Node> {
        let obj = await this.poolSys.createNode(ResourcesPathDefine.UI_ROOT_PATH + "/" + dlgConstructor.UI_PATH)
        obj["__is_pool"] = true;
        obj["__is_mul"] = true;
        if (data != null && data.parent) {
            obj.parent = data.parent;
        } else {
            obj.parent = this.root;
        }
        if (obj.getComponent(dlgConstructor) == null)
            obj.addComponent(dlgConstructor).initDlg(data);
        obj.getComponent(dlgConstructor).activeDlg(data);


        return obj;
    }

    async createDlg(dlgConstructor: typeof DlgBase, data?: any): Promise<Node> {
        if (this.dlgMap.has(dlgConstructor.UI_PATH)) {
            let obj = this.dlgMap.get(dlgConstructor.UI_PATH);
            if (data != null && data.hide) {
                obj.active = false;
            } else {
                obj.active = true;
                obj.getComponent(dlgConstructor).activeDlg(data);
            }
            if (data != null && data.parent) {
                obj.parent = data.parent;
            } else {
                obj.setSiblingIndex(999);
            }
            return obj
        }
            

        let obj = await ResourceLoader.insNode(ResourcesPathDefine.UI_ROOT_PATH + "/" + dlgConstructor.UI_PATH)
        this.dlgMap.set(dlgConstructor.UI_PATH, obj);
        if (data != null && data.parent) {
            obj.parent = data.parent;
        } else {
            obj.parent = this.root;
        }
        await obj.addComponent(dlgConstructor).initDlg(data);

        if (data != null && data.hide) {
            obj.active = false;
        } else {
            obj.active = true;
            obj.getComponent(dlgConstructor).activeDlg(data);
        }
        return obj;
    }

    getDlgCom<T extends DlgBase>(dlgConstructor: __private._types_globals__Constructor<T>): T | null {
        let dlg = this.dlgMap.get(dlgConstructor.UI_PATH)
        if (dlg == null)
            return null;
        return dlg.getComponent(dlgConstructor);
    }


    getDlg(dlgConstructor: typeof DlgBase): Node {
        return this.dlgMap.get(dlgConstructor.UI_PATH);
    }

    getDlgByName(dlgName: string): Node {
        return this.dlgMap.get(dlgName);
    }

    closeDlg(dlgConstructor: typeof DlgBase): void {
        this.closeDlgByPath(dlgConstructor.UI_PATH)
    }

    hideDlg(dlgConstructor: typeof DlgBase): void {
        this.hideDlgByPath(dlgConstructor.UI_PATH)
    }

    hideDlgByPath(UI_PATH: string): void {
        if (!this.dlgMap.has(UI_PATH))
            return;
        let oDlg = this.dlgMap.get(UI_PATH);
        oDlg.active = false;
    }

    closeDlgByPath(UI_PATH: string): void {
        if (!this.dlgMap.has(UI_PATH))
            return;
        let oDlg = this.dlgMap.get(UI_PATH);
        this.dlgMap.delete(UI_PATH);
        if (oDlg["__is_pool"]) {
            this.poolSys.destroyNode(oDlg)
        } else {
            oDlg.destroy();
        }
    }

}