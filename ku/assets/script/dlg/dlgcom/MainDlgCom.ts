﻿import { _decorator, Component, Node, Button, Label, tween, Vec3, Color, Sprite } from 'cc';
import { Functor } from '../../common/Functor';
import { SysMgr } from '../../common/SysMgr';
import { DlgSys } from '../../sys/DlgSys';

import { GlobalVar } from '../../common/GlobalVar';
import { BubbleTipDlg } from '../ui/BubbleTipDlg';
import { MainDlg } from '../ui/MainDlg';
import { ClickFunctor } from '../../common/ClickFunctor';
import { ResourceLoader } from '../../common/ResourceLoader';
import { UserProp } from '../../prop/UserProp';
import { EventSys } from '../../sys/EventSys';
import { PoolSys } from '../../sys/PoolSys';
import { FighterData } from '../../data/FighterData';
import { AudioSys } from '../../sys/AudioSys';
import { TimeSys } from '../../sys/TimeSys';
import { HttpUtils } from '../../utils/HttpUtils';
import { NSendCommon } from '../../net/send/NSendCommon';

const { ccclass, property } = _decorator;

@ccclass('MainDlgCom')
export class MainDlgCom extends MainDlg {
    

    initDlg(data?: any) {
        MainDlg.prototype.initDlg.call(this, data);
        this.m_StartButton.on("click", ClickFunctor.getFunc(this, "onStart"), this);

        UserProp.ins.watch("user_nack_name", this, "refreshNackName");
        SysMgr.getSys(EventSys).watchEvent("test", this, "onTest")

        //SysMgr.getSys(TimeSys).addTimer(this, "loop", 1, true);
    }


    loop() {
        //console.log("ttttttt")
    }

    onTest() {
        console.log("eeeeeeee")
    }

    refreshNackName() {
        this.m_TestLabel.getComponent(Label).string = UserProp.ins.user_nack_name
    }

    async onStart() {
        NSendCommon.send_user_nack_name("88888888")

        //SysMgr.getSys(TimeSys).removeTimer(this, "loop");
        //SysMgr.getSys(AudioSys).playAudio("audios/boom.mp3");

        //let fighterDataObject = FighterData.data.get(1)
        //console.log("EEEEEEEEEE", fighterDataObject.maxShieldEnergy)

        //console.log(SysMgr.getSys(PoolSys).objPool)
        //let obj1 = await SysMgr.getSys(PoolSys).createNode("prefabs/item/Sprite")
        //let obj2 = await SysMgr.getSys(PoolSys).createNode("prefabs/item/Sprite")
        //console.log(SysMgr.getSys(PoolSys).objPool)

        //await SysMgr.getSys(PoolSys).destroyNode(obj1)
        //console.log(SysMgr.getSys(PoolSys).objPool)

        //let obj3 = await SysMgr.getSys(PoolSys).createNode("prefabs/item/Sprite")
        //console.log(SysMgr.getSys(PoolSys).objPool)

        //let obj = await ResourceLoader.insNode("prefabs/item/Sprite")
        //obj.parent = this.node
        //obj.getComponent(Sprite).color = Color.GREEN;
        //this.closeDlg()
    }
 
}