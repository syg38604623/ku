﻿import { _decorator, Component, Node, Button, Label, tween, Vec3, Color, Sprite } from 'cc';
import { Functor } from '../../common/Functor';
import { SysMgr } from '../../common/SysMgr';
import { DlgSys } from '../../sys/DlgSys';

import { GlobalVar } from '../../common/GlobalVar';
import { BubbleTipDlg } from '../ui/BubbleTipDlg';

const { ccclass, property } = _decorator;

@ccclass('BubbleTipDlgCom')
export class BubbleTipDlgCom extends BubbleTipDlg {
    static msgArr: Array<string> = new Array<string>();

    static clear() {
        BubbleTipDlgCom.msgArr = new Array<string>();

    }

    static async createTip(msg: string) {
        let obj = await SysMgr.getSys(DlgSys).createMulPoolDlg(BubbleTipDlgCom, {
            msg: msg,
            parent: GlobalVar.tipPanel
        });
        obj.getComponent(BubbleTipDlgCom).refreshDlg({
            msg: msg,
            parent: GlobalVar.tipPanel
        })
    }

    static async _showTip() {
        if (BubbleTipDlgCom.msgArr.length <= 0)
            return;
        let msg = BubbleTipDlgCom.msgArr.shift();
        let obj = await SysMgr.getSys(DlgSys).createMulPoolDlg(BubbleTipDlgCom, {
            msg: msg,
            parent: GlobalVar.tipPanel
        });
        obj.getComponent(BubbleTipDlgCom).refreshDlg({
            msg: msg,
            parent: GlobalVar.tipPanel
        })
    }



    data?: any;
    nowPos: Vec3;
    moveUp = 30;

    stopTime = 0.5;
    moveTime = 0.35;
    colorChangeTime = 0.3;

    refreshDlg(data?: any) {
        this.node.position = new Vec3(0, 0, 0);
        this.m_TipLabel.getComponent(Label).string = data.msg;

        let tipLabel = this.m_TipLabel.getComponent(Label);
        let newColor = tipLabel.color.clone()
        newColor.a = 255;
        tipLabel.color = newColor;
        tween(newColor).delay(this.moveTime + this.stopTime).to(this.colorChangeTime, {
            a: 0
        }, {
            onUpdate: () => {
                tipLabel.color = newColor;
            }

        }).start();

        let bgSprite = this.m_BgSprite.getComponent(Sprite);
        let newColorSprite = bgSprite.color.clone();
        newColorSprite.a = 255;
        bgSprite.color = newColorSprite;
        tween(newColorSprite).delay(this.moveTime + this.stopTime).to(this.colorChangeTime, {
            a: 0
        }, {
            onUpdate: () => {
                bgSprite.color = newColorSprite;
            }

        }).start();


        this.nowPos = this.node.getPosition();
        let midPos = new Vec3(this.nowPos.x, this.nowPos.y + this.moveUp, this.nowPos.z);
        let targetPos = new Vec3(this.nowPos.x, this.nowPos.y + this.moveUp * 2, this.nowPos.z);

        tween(this.node).to(this.moveTime, {
            position: midPos,
        }).delay(this.stopTime).to(this.moveTime, {
            position: targetPos,
        }).call(Functor.getFunc(this, "removeDlg")).start();
    }

    initDlg(data?: any) {
        BubbleTipDlg.prototype.initDlg.call(this, data);

    }

    async removeDlg() {
        this.closeDlg();
        await BubbleTipDlgCom._showTip();
    }

 
}