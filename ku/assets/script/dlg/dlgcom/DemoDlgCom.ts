﻿import { _decorator, Component, Node, Button, Label, tween, Vec3, Color, Sprite, EditBox } from 'cc';
import { Functor } from '../../common/Functor';
import { SysMgr } from '../../common/SysMgr';
import { DlgSys } from '../../sys/DlgSys';

import { GlobalVar } from '../../common/GlobalVar';
import { BubbleTipDlg } from '../ui/BubbleTipDlg';
import { MainDlg } from '../ui/MainDlg';
import { ClickFunctor } from '../../common/ClickFunctor';
import { LoginDlg } from '../ui/LoginDlg';
import { UserProp } from '../../prop/UserProp';
import { EventSys } from '../../sys/EventSys';
import { HttpUtils } from '../../utils/HttpUtils';
import { WsSys } from '../../sys/WsSys';
import { MainDlgCom } from './MainDlgCom';
import { DemoDlg } from '../ui/DemoDlg';
import { WindowDlg } from '../ui/WindowDlg';
import { ResourceLoader } from '../../common/ResourceLoader';
import { BubbleTipDlgCom } from './BubbleTipDlgCom';
import { PoolSys } from '../../sys/PoolSys';
import { FighterData } from '../../data/FighterData';
import { AudioSys } from '../../sys/AudioSys';
import { TimeSys } from '../../sys/TimeSys';
import { WindowDlgCom } from './WindowDlgCom';

const { ccclass, property } = _decorator;

@ccclass('DemoDlgCom')
export class DemoDlgCom extends DemoDlg {

    resObj = null;
    poolObj = null;
    timeAdd = 0;

    initDlg(data?: any) {
        DemoDlg.prototype.initDlg.call(this, data);
        this.m_OpenButton.on("click", ClickFunctor.getFunc(this, "onOpenDlg"), this);


        this.m_ResAddButton.on("click", ClickFunctor.getFunc(this, "onResAdd"), this);
        this.m_ResDestroyButton.on("click", ClickFunctor.getFunc(this, "onResDestroy"), this);

        this.m_PropButton.on("click", ClickFunctor.getFunc(this, "onProp"), this);

        this.m_EventButton.on("click", ClickFunctor.getFunc(this, "onEvent"), this);

        this.m_PoolAddButton.on("click", ClickFunctor.getFunc(this, "onPoolAdd"), this);
        this.m_PollDestroyButton.on("click", ClickFunctor.getFunc(this, "onPoolDestroy"), this);

        this.m_ExcelButton.on("click", ClickFunctor.getFunc(this, "onExcel"), this);

        this.m_AudioButton.on("click", ClickFunctor.getFunc(this, "onAudio"), this);

        this.m_TimeAddButton.on("click", ClickFunctor.getFunc(this, "onTimeAdd"), this);
        this.m_TimeRemoveButton.on("click", ClickFunctor.getFunc(this, "onTimeRemove"), this);

        this.m_HttpButton.on("click", ClickFunctor.getFunc(this, "onHttp"), this);

        this.m_WsButton.on("click", ClickFunctor.getFunc(this, "onWs"), this);


        UserProp.ins.watch("user_name", this, "refreshName");//属性监听
        SysMgr.getSys(EventSys).watchEvent("test_event", this, "onTestEvent")//事件监听
    }

    async onOpenDlg() {
        await SysMgr.getSys(DlgSys).createDlg(WindowDlgCom)
    }

    async onResAdd() {
        if (this.resObj != null) return;
        this.resObj = await ResourceLoader.insNode("prefabs/item/Sprite")
        this.resObj.parent = this.m_ResPosNode
    }

    async onResDestroy() {
        if (this.resObj == null) return;
        this.resObj.destroy()
        this.resObj = null
    }

    async onProp() {
        UserProp.ins.setValue("user_name", "zhl0")
    }

    refreshName() {
        BubbleTipDlgCom.createTip("属性刷新成功")
        this.m_PropLabel.getComponent(Label).string = UserProp.ins.user_name
    }

    async onEvent() {
        SysMgr.getSys(EventSys).runEvent("test_event")
    }

    onTestEvent() {
        BubbleTipDlgCom.createTip("事件触发成功")
        this.m_EventLabel.getComponent(Label).string = "事件触发成功"
    }


    async onPoolAdd() {
        if (this.poolObj != null) return;
        this.poolObj = await SysMgr.getSys(PoolSys).createNode("prefabs/item/Sprite")
        this.poolObj.parent = this.m_PoolPosNode
    }


    async onPoolDestroy() {
        if (this.poolObj == null) return;
        await SysMgr.getSys(PoolSys).destroyNode(this.poolObj)
        this.poolObj = null
    }


    async onExcel() {
        let fighterDataObject = FighterData.data.get(0)
        this.m_ExcelLabel.getComponent(Label).string = fighterDataObject.fighterName
    }


    async onAudio() {
        await SysMgr.getSys(AudioSys).playAudio("audios/boom.mp3");
    }


    async onTimeAdd() {
        SysMgr.getSys(TimeSys).removeTimer(this, "loop");
        SysMgr.getSys(TimeSys).addTimer(this, "loop", 1, true);
    }

    loop() {
        this.m_TimeLabel.getComponent(Label).string = this.timeAdd.toString();
        this.timeAdd++;
    }


    async onTimeRemove() {
        SysMgr.getSys(TimeSys).removeTimer(this, "loop");
    }

    async onHttp() {
        let str = await HttpUtils.httpPost("noauth/test", JSON.stringify({
        }))
        let obj = JSON.parse(str)
        if (obj.code != 200) {
            BubbleTipDlgCom.createTip("服务异常")
            return;
        }
        this.m_HttpLabel.getComponent(Label).string = obj.data
    }


    async onWs() {
        let str = await HttpUtils.httpPost("noauth/login", JSON.stringify({
            "user_name": "456",
            "pwd": "456",
        }))

        let obj = JSON.parse(str)
        if (obj.code != 200) {
            BubbleTipDlgCom.createTip("服务异常")
            return;
        }

        let token = obj.data
        await SysMgr.getSys(WsSys).init(token)
        BubbleTipDlgCom.createTip("可查看心跳连接是否成功")
    }
}