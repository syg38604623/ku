﻿import { _decorator, Component, Node, Button, Label, tween, Vec3, Color, Sprite } from 'cc';
import { Functor } from '../../common/Functor';
import { SysMgr } from '../../common/SysMgr';
import { DlgSys } from '../../sys/DlgSys';

import { GlobalVar } from '../../common/GlobalVar';
import { BubbleTipDlg } from '../ui/BubbleTipDlg';
import { MainDlg } from '../ui/MainDlg';
import { ClickFunctor } from '../../common/ClickFunctor';
import { ResourceLoader } from '../../common/ResourceLoader';
import { UserProp } from '../../prop/UserProp';
import { EventSys } from '../../sys/EventSys';
import { PoolSys } from '../../sys/PoolSys';
import { FighterData } from '../../data/FighterData';
import { AudioSys } from '../../sys/AudioSys';
import { TimeSys } from '../../sys/TimeSys';
import { HttpUtils } from '../../utils/HttpUtils';
import { NSendCommon } from '../../net/send/NSendCommon';
import { WindowDlg } from '../ui/WindowDlg';

const { ccclass, property } = _decorator;

@ccclass('WindowDlgCom')
export class WindowDlgCom extends WindowDlg {
    

    initDlg(data?: any) {
        WindowDlg.prototype.initDlg.call(this, data);

        this.m_CloseButton.on("click", ClickFunctor.getFunc(this, "onClose"), this);
    }

    onClose() {
        this.closeDlg()
    }
}