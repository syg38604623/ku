﻿import { _decorator, Component, Node, Button, Label, tween, Vec3, Color, Sprite, EditBox } from 'cc';
import { Functor } from '../../common/Functor';
import { SysMgr } from '../../common/SysMgr';
import { DlgSys } from '../../sys/DlgSys';

import { GlobalVar } from '../../common/GlobalVar';
import { BubbleTipDlg } from '../ui/BubbleTipDlg';
import { MainDlg } from '../ui/MainDlg';
import { ClickFunctor } from '../../common/ClickFunctor';
import { LoginDlg } from '../ui/LoginDlg';
import { UserProp } from '../../prop/UserProp';
import { EventSys } from '../../sys/EventSys';
import { HttpUtils } from '../../utils/HttpUtils';
import { WsSys } from '../../sys/WsSys';
import { MainDlgCom } from './MainDlgCom';

const { ccclass, property } = _decorator;

@ccclass('LoginDlgCom')
export class LoginDlgCom extends LoginDlg {
    

    initDlg(data?: any) {
        LoginDlg.prototype.initDlg.call(this, data);
        this.m_LoginButton.on("click", ClickFunctor.getFunc(this, "onLogin"), this);
    }

    async onLogin() {
        let str = await HttpUtils.httpPost("noauth/login", JSON.stringify({
            "user_name": this.m_UserNameEditBox.getComponent(EditBox).string,
            "pwd": this.m_PwdEditBox.getComponent(EditBox).string,
        }))

        let obj = JSON.parse(str)
        if (obj.code != 200) {
            console.log(obj.msg)
            return;
        }
            
        let token = obj.data
        await SysMgr.getSys(WsSys).init(token)

        await SysMgr.getSys(DlgSys).createDlg(MainDlgCom);
        this.closeDlg();
        //SysMgr.getSys(EventSys).runEvent("test")
        //UserProp.ins.setValue("user_name", this.m_UserNameEditBox.getComponent(EditBox).string)
    }
 
}