﻿
import { _decorator, Component, Node, Button, Label } from 'cc';
import { DlgBase } from '../../base/DlgBase';

const { ccclass, property } = _decorator;

@ccclass('DemoDlg')
export class DemoDlg extends DlgBase {
    public static UI_PATH: string = "DemoDlg";
	public m_OpenButton: Node;
	public m_ResAddButton: Node;
	public m_ResPosNode: Node;
	public m_ResDestroyButton: Node;
	public m_PropButton: Node;
	public m_PropLabel: Node;
	public m_EventLabel: Node;
	public m_EventButton: Node;
	public m_PoolAddButton: Node;
	public m_PollDestroyButton: Node;
	public m_PoolPosNode: Node;
	public m_ExcelButton: Node;
	public m_ExcelLabel: Node;
	public m_AudioButton: Node;
	public m_TimeAddButton: Node;
	public m_TimeLabel: Node;
	public m_TimeRemoveButton: Node;
	public m_HttpButton: Node;
	public m_HttpLabel: Node;
	public m_WsButton: Node;

    initDlg(data?: any) {
		this.m_OpenButton = this.node.getChildByName("m_OpenButton");
		this.m_ResAddButton = this.node.getChildByName("m_ResAddButton");
		this.m_ResPosNode = this.node.getChildByName("m_ResDestroyButton").getChildByName("m_ResPosNode");
		this.m_ResDestroyButton = this.node.getChildByName("m_ResDestroyButton");
		this.m_PropButton = this.node.getChildByName("m_PropButton");
		this.m_PropLabel = this.node.getChildByName("m_PropLabel");
		this.m_EventLabel = this.node.getChildByName("m_EventLabel");
		this.m_EventButton = this.node.getChildByName("m_EventButton");
		this.m_PoolAddButton = this.node.getChildByName("m_PoolAddButton");
		this.m_PollDestroyButton = this.node.getChildByName("m_PollDestroyButton");
		this.m_PoolPosNode = this.node.getChildByName("m_PoolPosNode");
		this.m_ExcelButton = this.node.getChildByName("m_ExcelButton");
		this.m_ExcelLabel = this.node.getChildByName("m_ExcelLabel");
		this.m_AudioButton = this.node.getChildByName("m_AudioButton");
		this.m_TimeAddButton = this.node.getChildByName("m_TimeAddButton");
		this.m_TimeLabel = this.node.getChildByName("m_TimeLabel");
		this.m_TimeRemoveButton = this.node.getChildByName("m_TimeRemoveButton");
		this.m_HttpButton = this.node.getChildByName("m_HttpButton");
		this.m_HttpLabel = this.node.getChildByName("m_HttpLabel");
		this.m_WsButton = this.node.getChildByName("m_WsButton");

    }
}
