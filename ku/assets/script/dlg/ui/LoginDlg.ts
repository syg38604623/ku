﻿
import { _decorator, Component, Node, Button, Label } from 'cc';
import { DlgBase } from '../../base/DlgBase';

const { ccclass, property } = _decorator;

@ccclass('LoginDlg')
export class LoginDlg extends DlgBase {
    public static UI_PATH: string = "LoginDlg";
	public m_LoginButton: Node;
	public m_UserNameEditBox: Node;
	public m_PwdEditBox: Node;

    initDlg(data?: any) {
		this.m_LoginButton = this.node.getChildByName("m_LoginButton");
		this.m_UserNameEditBox = this.node.getChildByName("m_UserNameEditBox");
		this.m_PwdEditBox = this.node.getChildByName("m_PwdEditBox");

    }
}
