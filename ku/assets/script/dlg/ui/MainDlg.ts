﻿
import { _decorator, Component, Node, Button, Label } from 'cc';
import { DlgBase } from '../../base/DlgBase';

const { ccclass, property } = _decorator;

@ccclass('MainDlg')
export class MainDlg extends DlgBase {
    public static UI_PATH: string = "MainDlg";
	public m_StartButton: Node;
	public m_TestLabel: Node;

    initDlg(data?: any) {
		this.m_StartButton = this.node.getChildByName("m_StartButton");
		this.m_TestLabel = this.node.getChildByName("m_TestLabel");

    }
}
