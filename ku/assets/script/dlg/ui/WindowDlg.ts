﻿
import { _decorator, Component, Node, Button, Label } from 'cc';
import { DlgBase } from '../../base/DlgBase';

const { ccclass, property } = _decorator;

@ccclass('WindowDlg')
export class WindowDlg extends DlgBase {
    public static UI_PATH: string = "WindowDlg";
	public m_CloseButton: Node;

    initDlg(data?: any) {
		this.m_CloseButton = this.node.getChildByName("m_CloseButton");

    }
}
