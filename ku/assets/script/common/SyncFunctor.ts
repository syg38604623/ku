﻿import { _decorator, Component, Node, input, Input, EventKeyboard, KeyCode, Button } from 'cc';
import { PropBase } from '../base/PropBase';
import { AudioSys } from '../sys/AudioSys';
import { Functor } from './Functor';
import { SysMgr } from './SysMgr';
import { IntervalMgr } from './IntervalMgr';
import { SyncSys } from '../sys/SyncSys';


const { ccclass, property } = _decorator;

@ccclass('SyncFunctor')
export class SyncFunctor {
    functor: Functor;
    
    constructor(functor: Functor) {
        this.functor = functor;
    }

    runFunc(...arg: any) {
        SysMgr.getSys(SyncSys).addSyncFunctor(this.runCbFunc.bind(this), arg);
    }

    async runCbFunc(...arg: any) {
        if (this.functor != null)
            await this.functor.runCbFunc(...arg);
    }
    static getFunc(obj: any, funName: string, ...arg: any) {

        let functor = new Functor(obj, funName, ...arg);
        let syncFunctor = new SyncFunctor(functor);
        return syncFunctor.runFunc.bind(syncFunctor);
    }
}

