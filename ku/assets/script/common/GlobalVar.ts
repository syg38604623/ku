﻿import { _decorator, Color, Component, Node } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('GlobalVar')
export class GlobalVar {
    static isTest = false;
    static isWX = false;
    static isAndroid = false;
    static isMobile = false;

    static isRestart = false;
    static canvas: Node;
    static tipPanel: Node;
    static topPanel: Node;



    //static server_url = "http://127.0.0.1:6554/"
    //static ws_url = "ws://127.0.0.1:6554/ws/"

    static server_url = "https://fahuang.fun/backend/ku/api/"
    static ws_url = "wss://fahuang.fun/backend/ku/ws/ws/"

}

