﻿import { _decorator, Component, Node , native, Vec2, Vec3 } from 'cc';
import {HttpUtils} from "../utils/HttpUtils";
import { Defines } from './Defines';


const { ccclass, property } = _decorator;

@ccclass('Misc')
export class Misc {

    static cloneObjToProp(obj, prop) {
        let obj_key_list = Object.keys(obj)
        for (let j = 0, obj_key_len = obj_key_list.length; j < obj_key_len; j++) {
            let obj_key = obj_key_list[j]
            prop[obj_key] = obj[obj_key]
        }
    }

    static cloneObjToMapProp(mapObj, map, cls) {
        let key_list = Object.keys(mapObj)

        for (let i = 0, len = key_list.length; i < len; i++) {
            let key = key_list[i]
            let obj = mapObj[key]
            let prop = new cls();
            Misc.cloneObjToProp(obj, prop)
            map.set(key.toString(), prop)
        }
    }


    static deepCopyArray<T>(arr: Array<T>): Array<T> {
        let copiedArray = [];
        for (let i = 0; i < arr.length; i++) {
            if (Array.isArray(arr[i])) {
                copiedArray.push(Misc.deepCopyArray(arr[i]));
            } else {
                copiedArray.push(arr[i]);
            }
        }
        return copiedArray;
    }



    static mapToString(map): string {
        let ro = {};
        for (var key of map.keys()) {
            ro[key] = map.get(key);
      
        }
        return JSON.stringify(ro);
    }


    static objToMap(obj) {
        var map = new Map();
        for (const key in obj) {
            var value = obj[key];
            map.set(key, value);
        };
        return map;
    }

 

    static addAngleDir(dir: Vec2, angle: number) {
        angle = (angle) * Math.PI / 180;
        return new Vec2(dir.x * Math.cos(angle) - dir.y * Math.sin(angle), dir.x * Math.sin(angle) + dir.y * Math.cos(angle))
    }
    static removeListByIndex<T>(index: number, list: Array<T>): T {
        var arr = list.splice(index, 1);
        return arr[0];
    }
    static removeListByObj<T>(obj: T, list: Array<T>): T {
        for (var i = 0; i < list.length; i++) {
            if (list[i] == obj) {
                this.removeListByIndex(i, list);
                return obj;
            }
        }
        return null;
    }

    static sub(v1: Vec3, v2: Vec3): Vec3 {
        return new Vec3(0, 0, 0).add(v1).subtract(v2);
    }
    static add(v1: Vec3, v2: Vec3): Vec3 {
        return new Vec3(0, 0, 0).add(v1).add(v2);
    }
    static mulScale(v1: Vec3, scale: number): Vec3 {
        return new Vec3(0, 0, 0).add(v1).multiplyScalar(scale);
    }
}

