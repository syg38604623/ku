﻿import { _decorator, Component, Node , native } from 'cc';
import {HttpUtils} from "../utils/HttpUtils";
const { ccclass, property } = _decorator;

@ccclass('ResourcesPathDefine')
export class ResourcesPathDefine {

    static UI_ROOT_PATH = "prefabs/ui";



    static SOUND_MAP: Map<number, Array<any>> = new Map([
        [0, ["audios/attack0.wav",0.4]],
    ])

}

