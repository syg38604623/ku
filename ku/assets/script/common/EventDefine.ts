﻿import { _decorator, Component, Node , native } from 'cc';
import {HttpUtils} from "../utils/HttpUtils";
const { ccclass, property } = _decorator;

@ccclass('EventDefine')
export class EventDefine {
    static PLAYER_RECORD_CHANGE = "PLAYER_RECORD_CHANGE";
}

