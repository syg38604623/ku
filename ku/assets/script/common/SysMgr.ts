﻿import { _decorator, Component, Node, __private } from 'cc';
import { SysBase } from '../base/SysBase';
import { AudioSys } from '../sys/AudioSys';
import { CommonSys } from '../sys/CommonSys';
import { DlgSys } from '../sys/DlgSys';
import { EventSys } from '../sys/EventSys';
import { GameSys } from '../sys/GameSys';

import { PoolSys } from '../sys/PoolSys';
import { TimeSys } from '../sys/TimeSys';
import { UserProp } from '../prop/UserProp';
import { WsSys } from '../sys/WsSys';
import { SyncSys } from '../sys/SyncSys';
import { MainDlgCom } from '../dlg/dlgcom/MainDlgCom';
import { LoginDlgCom } from '../dlg/dlgcom/LoginDlgCom';
import { DemoDlgCom } from '../dlg/dlgcom/DemoDlgCom';




const { ccclass, property } = _decorator;

@ccclass('SysMgr')
export class SysMgr extends SysBase {
    public static ins: SysMgr;

    static getSys<T extends Component>(classConstructor: __private._types_globals__Constructor<T> | __private._types_globals__AbstractedConstructor<T>): T | null {
        return SysMgr.ins.getCom(classConstructor);
    }

    static addSys<T extends SysBase>(classConstructor: __private._types_globals__Constructor<T>): T | null {
        return SysMgr.ins.addCom(classConstructor);
    }

    static removeSys<T extends SysBase>(classConstructor: __private._types_globals__Constructor<T> | __private._types_globals__AbstractedConstructor<T>): void {
        SysMgr.ins.removeCom(classConstructor);
    }


    async init() {
        UserProp.init();

        SysMgr.addSys(TimeSys);
        SysMgr.addSys(SyncSys);
        SysMgr.addSys(EventSys);
        SysMgr.addSys(CommonSys);
        SysMgr.addSys(PoolSys);
        SysMgr.addSys(DlgSys);

        SysMgr.addSys(AudioSys);
        SysMgr.addSys(GameSys);
        SysMgr.addSys(WsSys);

        return this;
    }

    async enter() {
        await SysMgr.getSys(DlgSys).createDlg(DemoDlgCom);
    }

    async enterMain() {


    }

}
