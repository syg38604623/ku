﻿import { _decorator, Component, Node, Animation, __private, Vec3, ProgressBar, tween } from 'cc';
import { Functor } from './Functor';
import { DestroyCB } from './DestroyCB';
import { ResourcesObjBase } from '../base/ResourcesObjBase';


const { ccclass, property } = _decorator;

@ccclass('TweenPlus')
export class TweenPlus extends Component {
    static _tween_plus_action_set = "_tween_plus_action_set";
    static tween(node: Node) {
        if (node.getComponent(ResourcesObjBase) == null)
            node.addComponent(ResourcesObjBase)
        return this.create(node, node.getComponent(ResourcesObjBase))
    }


    static create(obj: any, watchDestroyObj: any) {
        let t = tween(obj)
        if (watchDestroyObj[this._tween_plus_action_set] == null) {
            watchDestroyObj[this._tween_plus_action_set] = new Set<any>();
        }
        watchDestroyObj[this._tween_plus_action_set].add(t)
        
        DestroyCB.addCb(watchDestroyObj, this, "_destroyNode")
        return t
    }


    static _destroyNode(watchDestroyObj) {
        let tweenSet = watchDestroyObj[this._tween_plus_action_set]
        for (let t of tweenSet)
            t.stop();
    }
   
}

