﻿import { _decorator, Component, Node , native, Vec3, SpriteFrame } from 'cc';
import { BubbleTipDlgCom } from '../dlg/dlgcom/BubbleTipDlgCom';
import { PoolSys } from '../sys/PoolSys';
import {HttpUtils} from "../utils/HttpUtils";
import { ResourceLoader } from './ResourceLoader';
import { ResourcesPathDefine } from './ResourcesPathDefine';
import { SysMgr } from './SysMgr';

import { GlobalVar } from './GlobalVar';
const { ccclass, property } = _decorator;

@ccclass('PreLoad')
export class PreLoad {

    static resArr: Array<string> = [
        ResourcesPathDefine.UI_ROOT_PATH + "/" + BubbleTipDlgCom.UI_PATH,
        "audios/touch.mp3",
    ];



    static async preload() {
        for (let resPath of PreLoad.resArr) {
            await ResourceLoader.loadAsset(resPath);
        }
    }


}

