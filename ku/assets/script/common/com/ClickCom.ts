﻿import { _decorator, Component, Node, input, Input, Animation, EventKeyboard, KeyCode, loader, assetManager, Texture2D, SpriteFrame, Sprite, NodeEventType, EventTouch, Vec3, Vec2 } from 'cc';
import { ComBase } from '../../base/ComBase';
import { PoolSys } from '../../sys/PoolSys';
import { TimeSys } from '../../sys/TimeSys';
import { Functor } from '../Functor';
import { ResourceLoader } from '../ResourceLoader';
import { ResourcesPathDefine } from '../ResourcesPathDefine';
import { SysMgr } from '../SysMgr';
import { IntervalMgr } from '../IntervalMgr';


const { ccclass, property } = _decorator;

@ccclass('ClickCom')
export class ClickCom extends ComBase {
    cbFunc: Function;
    isStart = false;
    timeSys: TimeSys;
    startPos = null;
    init(cbFunc: Function) {
        this.cbFunc = cbFunc;
        this.node.on(NodeEventType.TOUCH_START, Functor.getFunc(this, "onTouchStart"), this);
        this.node.on(NodeEventType.TOUCH_MOVE, Functor.getFunc(this, "onTouchMove"), this);
        this.node.on(NodeEventType.TOUCH_END, Functor.getFunc(this, "onTouchEnd"), this);
        this.node.on(NodeEventType.TOUCH_CANCEL, Functor.getFunc(this, "onTouchEnd"), this);
        this.timeSys = SysMgr.getSys(TimeSys);
    }

    @IntervalMgr.Lock()
    async onTouchStart(event: EventTouch) {
        await this.callStartFunc();
    }

    async callStartFunc() {
        await this.cbFunc(this.node);
    }

    onTouchEnd(event: EventTouch) {

    }
}

