﻿import { _decorator, Component, Node, input, Input, EventKeyboard, KeyCode, game, loader, assetManager, Texture2D, sys, TextAsset } from 'cc';
import { ComBase } from '../../base/ComBase';
import { Defines } from '../Defines';
import { GlobalVar } from '../GlobalVar';
import { ResourceLoader } from '../ResourceLoader';
import { ResourcesPathDefine } from '../ResourcesPathDefine';
import { SysMgr } from '../SysMgr';
import { CodeReoladTool } from '../../tools/CodeReoladTool';
import { BubbleTipDlgCom } from '../../dlg/dlgcom/BubbleTipDlgCom';


const { ccclass, property } = _decorator;

@ccclass('KeyCom')
export class KeyCom extends ComBase {
    ctrlDown = false;
    funcDict: Map<number, Function> = new Map([
        [KeyCode.KEY_D, KeyCom.prototype.reload],
    ])
    onLoad() {
        input.on(Input.EventType.KEY_DOWN, this.onKeyDown, this);
        input.on(Input.EventType.KEY_UP, this.onKeyUp, this);
    }
    async reload() {
        CodeReoladTool.reload()
        await BubbleTipDlgCom.createTip("重载完成")
    }

    async onKeyDown(event: EventKeyboard) {
        if (event.keyCode == KeyCode.CTRL_LEFT) {
            this.ctrlDown = true;
        }
       
        if (this.funcDict.has(event.keyCode)) {
            await this.funcDict.get(event.keyCode).bind(this)(this.ctrlDown);
        }
    }

    onKeyUp(event: EventKeyboard) {
        if (event.keyCode == KeyCode.CTRL_LEFT) {
            this.ctrlDown = false;
        }
    }
}

