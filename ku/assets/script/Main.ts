﻿import { _decorator, Component, Node, native, Label, input, TypeScript, CCClass, js, Input, EventKeyboard, loader, sys, view, macro } from 'cc';

import { GlobalVar } from './common/GlobalVar';
import { SysMgr } from './common/SysMgr';
import { CodeReoladTool } from './tools/CodeReoladTool';




const { ccclass, property } = _decorator;

@ccclass('Main')
export class Main extends Component {
    async onLoad() {
        this.initGlobalVar();
        this.initSys();

    }

    initGlobalVar() {
        if (!window["wx"])
            GlobalVar.isWX = false;
        else {
            GlobalVar.isWX = true;
        }
        if (sys.platform == sys.Platform.ANDROID) {
            GlobalVar.isAndroid = true;
        }
        GlobalVar.isMobile = sys.isMobile

       
        GlobalVar.isRestart = false;
        GlobalVar.canvas = this.node;
        GlobalVar.tipPanel = this.node.getChildByName("tipPanel");
        GlobalVar.topPanel = this.node.getChildByName("topPanel");

    }

    async initSys() {
        await CodeReoladTool.init()
        var sysMgrObj = new Node();
        sysMgrObj.name = SysMgr.name;
        sysMgrObj.parent = this.node;

        SysMgr.ins = sysMgrObj.addComponent(SysMgr);
        await SysMgr.ins.init();

        await SysMgr.ins.enter();
    }
}
