﻿import { _decorator, Component, Node, resources } from 'cc';
import { Defines } from '../common/Defines';
import { GlobalVar } from '../common/GlobalVar';

const { ccclass, property } = _decorator;

@ccclass('WXTool')
export class WXTool {

	static share(succFunc?: Function, failFunc?: Function) {
		if (!GlobalVar.isWX) return;
		resources.load("textures/other/icon", function (err, data) {
			window["wx"].shareAppMessage({
				title: "穿越幻想，踏入神域，开启史诗之旅！",
				imageUrl: data.url,
				withShareTicket: true,
				success: res => {
					succFunc()
				},
				fail: resp => {
					failFunc()
				},
			});

		})

	}

	static uploadScore(score: number) {//只会上传比存储分值高的分数
		if (!GlobalVar.isWX) return;
		window["wx"].postMessage({ command: "uploadScore", score: score });
	}

	static setScore(score: number) {//直接设置分数
		if (!GlobalVar.isWX) return;
		window["wx"].postMessage({ command: "setScore", score: score });
	}

	static showRank() {
		if (!GlobalVar.isWX) return;
		window["wx"].postMessage({ command: "render" });
	}

	static closeRank() {
		if (!GlobalVar.isWX) return;
		window["wx"].postMessage({ command: "close" });
    }

}
