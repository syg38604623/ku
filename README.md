## 前端基本功能
### 1. 界面系统的使用
- 打开界面
```
SysMgr.getSys(DlgSys).createDlg(HotfixDlgCom);
```
- 关闭界面
```
this.closeDlg()
await SysMgr.getSys(DlgSys).closeDlg(HotfixDlgCom);
```



### 2. 热重载系统使用（适用于大部分代码）
- 修改代码之后，按ctrl+D直接生效
- 仅仅能热重载函数，对于成员变量的修改不生效
- 仅适用于js支持的语法

### 3.资源管理
```
let obj = await ResourceLoader.insNode("prefabs/item/Sprite")
obj.parent = this.node
obj.getComponent(Sprite).color = Color.RED;
```

### 4.属性监听回调
- 监听属性值
```
 UserProp.ins.watch("user_name", this, "refreshName");
```
- 刷新属性值
```
UserProp.ins.setValue("user_name","zhl")
```





### 5.消息（事件）管理
- 监听事件
```
SysMgr.getSys(EventSys).watchEvent("add_node", this, "onItemAdd")
```
- 发送事件
```
SysMgr.getSys(EventSys).runEvent("add_node")
```



### 6.对象池使用
- 创建（或者从对象池中获取）节点
```
 let obj = await SysMgr.getSys(PoolSys).createNode("prefabs/item/Sprite")
 obj.parent = this.node
 obj.getComponent(Sprite).color = Color.RED;
```


### 7.导表功能
- 使用工具设置好路径，解析excel文件生成data文件到对应目录
```
let fighterDataObject = FighterData.data.get(0)
```




### 8.音效系统
```
SysMgr.getSys(AudioSys).playAudio("audios/boom.mp3");
```

### 9.定时系统
```
SysMgr.getSys(TimeSys).addTimer(this, "loop", 1, true);
```




## 后端基本配置
### 1.修改配置文件
- 修改数据库用户名
- 修改数据库密码
- 修改服务器ip
- 修改服务器端口
- 是否使用热重启
```
{
  "MYSQL_USER": "root",
  "MYSQL_PWD": "root",
  "SERVER_IP": "127.0.0.1",
  "SERVER_PORT": 6554,
  "RELOAD": true
}
```



## 后端基本功能
### 1.Http接口实现
- 添加无token校验接口（noauth模块的请求都不校验token）
```
@router.post('/test')
@unified_resp
async def test(params: dict):
	print(f"AAAAAAA {params}")

```

### 2.数据库的增删改查
- 增

```
await sqlplus.insert(User, {
		"user_name":user_name,
		"user_nack_name":random_util.get_random_name(),
		"pwd": auth_util.md5(pwd)
	})
```

- 删

```
await sqlplus.delete(User, [User.id == user_id, ])
```

- 改

```
await sqlplus.update(User, {
			"user_name": user_name
		}, [User.id == user_id, ])
```

- 查

```
dUser = await sqlplus.getOne(User, [User.id == user_id, ])
```






### 3.Websocket协议增加

```
class HandleCommon(SingletonBase):
	async def heart(self, user_id, data):
		await UserMgr.getObj().heartUser(user_id)
```

## Http前后端交互
### 1.注册
- 前端注册界面

```
initDlg(data?: any) {
    RegisterDlg.prototype.initDlg.call(this, data);
    this.m_RegisterBtn.on("click", ClickFunctor.getFunc(this, "onRegister"), this);
}

async onRegister() {
    let user_name = this.m_UserNameEditBox.getComponent(EditBox).string;
    let pwd = this.m_PwdEditBox.getComponent(EditBox).string;
    let pwd2 = this.m_PwdEditBox.getComponent(EditBox).string;
    let data = await HttpUtils.httpPost("noauth/register", JSON.stringify({
        user_name: user_name,
        pwd: pwd,
        pwd2: pwd2
    }))
    console.log(data)
    this.closeDlg()
}
```

- 后端注册接口

```
@router.post('/register')
@unified_resp
@db.transaction()
async def register(data: dict):
	user_name = data.get("user_name")
	pwd = data.get("pwd")
	pwd2 = data.get("pwd2")
	
	if misc.is_empty(user_name):
		raise AppException(HttpResp.FAILED, msg='用户名不能为空!')
	
	if misc.is_empty(pwd):
		raise AppException(HttpResp.FAILED, msg='密码不能为空!')
	
	if pwd != pwd2:
		raise AppException(HttpResp.FAILED, msg='密码不一致!')
	
	dUser = await sqlplus.getOne(User, [User.user_name == user_name, ])
	if dUser is not None:
		raise AppException(HttpResp.FAILED, msg='用户已存在!')
	
	await sqlplus.insert(User, {
		"user_name":user_name,
		"user_nack_name":random_util.get_random_name(),
		"pwd": auth_util.md5(pwd)
	})
	return True
```


### 2.登录

- 前端登录界面

```
initDlg(data?: any) {
    LoginDlg.prototype.initDlg.call(this, data);
    this.m_LoginBtn.on("click", ClickFunctor.getFunc(this, "onLogin"), this);

}

async onLogin() {
    let user_name = this.m_UserNameEditBox.getComponent(EditBox).string;
    let pwd = this.m_PwdEditBox.getComponent(EditBox).string;
    let data = await HttpUtils.httpPost("noauth/login", JSON.stringify({
        user_name: user_name,
        pwd:pwd
    }))
    let obj = JSON.parse(data)
    let token = obj.data
    SysMgr.getSys(WsSys).init(token)
}

```

- 后端登录接口

```
@router.post('/login')
@unified_resp
async def login(data: dict):
	user_name = data["user_name"]
	pwd = data["pwd"]
	
	if misc.is_empty(user_name):
		raise AppException(HttpResp.FAILED, msg='用户名不能为空!')
	
	if misc.is_empty(pwd):
		raise AppException(HttpResp.FAILED, msg='密码不能为空!')
	
	dUser = await sqlplus.getOne(User, [User.user_name == user_name, ])
	
	if dUser is None:
		raise AppException(HttpResp.FAILED, msg='用户不存在!')
	if dUser["pwd"] != auth_util.md5(pwd):
		raise AppException(HttpResp.FAILED, msg='密码错误')

	return auth_util.generate_token({"user_id": dUser["id"]})

```


## Websocket前后端交互
### 1. 用获取的token初始化websocket

```
SysMgr.getSys(WsSys).init(token)
```

### 2.增加前端发送协议

```
@ccclass('NSendCommon')
export class NSendCommon {
    static send_user_nack_name(user_nack_name) {
        SysMgr.getSys(WsSys).sendMsg(Defines.C2S_COMMON, "recv_user_nack_name", {
            user_nack_name: user_nack_name
        });
    }
}
```

### 3.增加后端接收协议

```
class HandleCommon(SingletonBase):
	async def recv_user_nack_name(self, user_id, data):
		user_nack_name = data["user_nack_name"]
		await sqlplus.update(User, {
			"id":user_id,
			"user_nack_name":user_nack_name
		})
		
		await sender_common.send_user_nack_name(user_id, user_nack_name)
```

### 4.增加后端发送协议

```
async def send_user_nack_name(user_id, user_nack_name):
	await misc.send_data(user_id, defines.S2C_COMMON, "recvUserNackName", {
		"user_nack_name": user_nack_name,
	})
```

### 5.增加前端接收协议

```
@ccclass('NRecvCommon')
export class NRecvCommon {
    static async recvUserNackName(data) {
        UserProp.ins.setValue("user_nack_name", data.user_nack_name)
    }
}
```



## 框架更多实践案例
|项目名|类型|前端功能|后端功能|
|-|-|-|-|
|[![](https://download-cn.cocos.com/CocosStore/resource/2023-10-05/sys/b1ddc84c11bd4cdcb5b28abd59fbcc04/b1ddc84c11bd4cdcb5b28abd59fbcc04.png)](https://store.cocos.com/app/detail/6085) <br>[**《绯色神域online》联机对战卡牌回合制游戏**](https://store.cocos.com/app/detail/6085)|卡牌、回合制、联机对战、二次元|界面系统、资源管理、属性监听回调、消息（事件）管理、对象池、导表配置、定时系统、音效系统、长连接管理系统、**指引系统**、**红点系统**、**动画管理**、**战斗系统**...| 数据库基本操作、FastApi集成（Http服务）、Websocket集成、定时系统、**战斗逻辑管理**、**房间系统**、**英雄管理系统**、**用户管理系统**、**排行榜**、**签到系统**、**布阵管理**、**AI管理**、**商城管理**、**匹配对战**...
|[![](https://download-cn.cocos.com/CocosStore/resource/2024-01-15/sys/0e12bb9ea35744ff86b09ee6f4952054/0e12bb9ea35744ff86b09ee6f4952054.png)](https://store.cocos.com/app/detail/5797) <br>[**《幻境之巅》联机对战 消除类 回合制游戏**](https://store.cocos.com/app/detail/5797)|转珠、卡牌、回合制、联机对战、二次元|**高级界面系统**、资源管理、属性监听回调、消息（事件）管理、对象池、导表配置、定时系统、音效系统、长连接管理系统、**指引系统**、**红点系统**、**动画管理系统**、**循环滚动框**、**战斗系统**...| 数据库基本操作、FastApi集成（Http服务）、Websocket集成、定时系统、**战斗逻辑管理**、**英雄管理系统**、**用户管理系统**、**排行榜**、**签到系统**、**布阵管理**、**活动系统**、**好友系统**、**符文系统**、**商城管理**、**AI管理**、**邮件系统**、**打造系统**、**背包管理**、**提炼管理**、**匹配对战**...
|[![](https://download.cocos.com/CocosStore/resource/69d8e8b81ed14f38b7c78257e252b685/69d8e8b81ed14f38b7c78257e252b685.png)](https://store.cocos.com/app/detail/6556) <br>[**《星际征服者》3D实时对战 战机类游戏**](https://store.cocos.com/app/detail/6556)|3D联机对战、战机类、多人在线、射击|界面系统、资源管理、属性监听回调、消息（事件）管理、对象池、导表配置、定时系统、音效系统、长连接管理系统、**动画管理系统**、**相机管理系统**、**战斗系统**...| 数据库基本操作、FastApi集成（Http服务）、Websocket集成、定时系统、**战斗逻辑管理**、**战机管理系统**、**用户管理系统**、**房间系统**...
|[![](https://download-cn.cocos.com/CocosStore/resource/2023-10-05/sys/b1ddc84c11bd4cdcb5b28abd59fbcc04/b1ddc84c11bd4cdcb5b28abd59fbcc04.png)](https://store.cocos.com/app/detail/5368) <br>[**《绯色神域》ai生成的卡牌回合制游戏**](https://store.cocos.com/app/detail/5368)|卡牌、回合制、联机对战、二次元|界面系统、资源管理、属性监听回调、消息（事件）管理、对象池、导表配置、定时系统、音效系统、**指引系统**、**红点系统**、**动画管理**、**战斗系统**...| 无后端功能
|[![](https://download-cn.cocos.com/CocosStore/resource/2023-11-18/sys/38426ca4b75447d6a4eb822fce6ca7a9/38426ca4b75447d6a4eb822fce6ca7a9.png)](https://store.cocos.com/app/detail/5544)<br>[**喵喵退退退**](https://store.cocos.com/app/detail/5544)|卡牌、割草、肉鸽、塔防、射击、弹幕|界面系统、资源管理、属性监听回调、消息（事件）管理、对象池、导表配置、定时系统、音效系统、**动画管理**、**战斗系统**...| 无后端功能
|[![](https://download-cn.cocos.com/CocosStore/resource/2023-11-06/sys/c6cf6d707d0a43e78142b7f2271dd020/c6cf6d707d0a43e78142b7f2271dd020.png)](https://store.cocos.com/app/detail/5495) <br>[**几何武装蛇**](https://store.cocos.com/app/detail/5495)|割草、合成、益智、弹幕|界面系统、资源管理、属性监听回调、消息（事件）管理、对象池、导表配置、定时系统、音效系统、**动画管理**、**点击管理系统**、**战斗系统**...| 无后端功能
|[![](https://download.cocos.com/CocosStore/icon/af79ccbf01754e26b94de382769acdd8/af79ccbf01754e26b94de382769acdd8.png)](https://store.cocos.com/app/detail/4874) <br>[**文字斗争**](https://store.cocos.com/app/detail/4874) |文字、策略、益智|界面系统、资源管理、属性监听回调、消息（事件）管理、对象池、导表配置、定时系统、音效系统、**动画管理**、**战斗系统**、**AI系统**...| 无后端功能
|[![](https://download.cocos.com/CocosStore/resource/bb89c512a5ff49e09e4bf0bca3fd1398/bb89c512a5ff49e09e4bf0bca3fd1398.png)](https://store.cocos.com/app/detail/6384)<br>[**3D俄罗斯方块**](https://store.cocos.com/app/detail/6384)|3D、益智、休闲|界面系统、资源管理、属性监听回调、消息（事件）管理、对象池、定时系统、音效系统...| 无后端功能





